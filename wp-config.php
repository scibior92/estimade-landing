<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'estimade_landing' );

/** MySQL database username */
define( 'DB_USER', 'estimade' );

/** MySQL database password */
define( 'DB_PASSWORD', 'SuperSecret123@' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '9UF{tJ4KH.&U8HKmNHi``V}W C!PV&|l--rXH7Y[y2@yT&~C|Rs|(`Ar:JlZ6dZ8' );
define( 'SECURE_AUTH_KEY',  'ITjJ=p~1H,(i?hb,nNW~0tTl*}}OE;FSu|)7U>UEu}x^X(A@omM}PS77c*g,,nS)' );
define( 'LOGGED_IN_KEY',    '*pUj#rpXcW{L1mD*G;:&|;1:B}r]gH)b^x@Z(_`l{lYS848;t]%E.4;:O7v cLtC' );
define( 'NONCE_KEY',        'dz#HHM,c)*J(fM9I[~WPX54S+@n{H^<}?0fy~8/)i^XOo&>5(2r;hLJ})+>46G|O' );
define( 'AUTH_SALT',        'm1)b7>|=kb8p&XN8FVYRe.<niZ|#hng>Xr]4X,T+B9riy)q-[>Op).q`cCXC2==u' );
define( 'SECURE_AUTH_SALT', 'ih_E!d/Dy+4+!G=Y<eORA6(&kP[)&mj177h)&,;PcVOcq%fW6Ld$x1_V}iP>fVjP' );
define( 'LOGGED_IN_SALT',   '}x`{S(}brnfTT&IoRSoU`G#zC-3XyGO>ldo=c$1EQ%6#*B=W(ANK2{XtL Jw]!YP' );
define( 'NONCE_SALT',       '2X|4Tzd`WJjb4Bqr5_-v%R>J8u{Ie-`gnM.IVTFun^kRz7/6$tzn}_EpA@gk%5;f' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
