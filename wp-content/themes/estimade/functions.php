<?php

function register_acf_options_pages()
{

    // check function exists
    if (!function_exists('acf_add_options_page')) {
        return;
    }
    // register options page
    $my_options_page = acf_add_options_page(
        array(
            'page_title' => __('My Options Page'),
            'menu_title' => __('My Options Page'),
            'menu_slug' => 'my-options-page',
            'capability' => 'edit_posts',
            'show_in_graphql' => true,
        )
    );
}

add_action('acf/init', 'register_acf_options_pages');

if (!function_exists('unwind_setup')) :
    function unwind_setup()
    {
        register_nav_menus(
            array(
                'primary' => __('Primary', 'unwind'),
                'footer' => __('Footer', 'unwind'),
            )
        );

    }
endif;
add_action('after_setup_theme', 'unwind_setup');


function my_custom_mime_types($mimes)
{
    $mimes['svg'] = 'image / svg + xml';
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'my_custom_mime_types');
add_filter( 'big_image_size_threshold', '__return_false' );

add_theme_support('post-thumbnails');

/* add_action('publish_future_post', 'nb_webhook_future_post', 10);
add_action('publish_post', 'nb_webhook_post', 10, 2);
add_action('publish_page', 'nb_webhook_post', 10, 2);
add_action('post_updated', 'nb_webhook_update', 10, 3); */

function nb_webhook_future_post($post_id)
{
    nb_webhook_post($post_id, get_post($post_id));
}

function nb_webhook_update($post_id, $post_after, $post_before)
{
    nb_webhook_post($post_id, $post_after);
}

function nb_webhook_post($post_id, $post)
{
    if ($post->post_status === 'publish') {
        $url = curl_init('https://api.netlify.com/build_hooks/{my-build-hook}'); // TODO: Change this URL!!
        curl_setopt($url, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($url, CURLOPT_RETURNTRANSFER, true);
        curl_exec($url);
    }
}

add_action('init', function() {

    function getFormData(WP_REST_Request $request) {
        $data = [];
        $data['return'] = true;
        return new WP_REST_Response( $data, 200 );
    }
});

add_action( 'rest_api_init', function () {
    //register_rest_route( 'forms/', '/id/(?P<id>\d+)', array(
    register_rest_route( 'forms/', '', array(
        'methods' => 'GET',
        'callback' => 'getFormData',
    ));
});